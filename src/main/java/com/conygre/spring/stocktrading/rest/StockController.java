package com.conygre.spring.stocktrading.rest;

import com.conygre.spring.stocktrading.service.StockService;

import java.util.List;

//import java.util.Optional;

import com.conygre.spring.stocktrading.entities.Stock;

//import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/stock")
@CrossOrigin // allows requests from all domains
public class StockController {

    @Autowired
	private StockService service;

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Stock> findAll() {
		return service.getAllStocks();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public List <Stock> getStockById(@PathVariable("id") String id) {
		return service.getStockById(id);
	}


	@RequestMapping(method = RequestMethod.POST)
	public void addStock(@RequestBody Stock stock) {
		service.addToStockDB(stock);
	}
}
