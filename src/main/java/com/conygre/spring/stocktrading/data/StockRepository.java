package com.conygre.spring.stocktrading.data;

import com.conygre.spring.stocktrading.entities.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface StockRepository extends MongoRepository<Stock, ObjectId> {

	public List<Stock> findByTicker(String ticker);

	
}