package com.conygre.spring.stocktrading.dao;

import com.conygre.spring.stocktrading.entities.Stock;

import java.util.Collection;

public interface StockDAO {
    
    void addStock(Stock stock);
    Stock getStockByTicker(String title);
    Collection<Stock> getAllStocks();
}
